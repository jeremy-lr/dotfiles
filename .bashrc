
autoload -U bashcompinit
bashcompinit

export PATH=$PATH:/d/Java/apache-maven-3.3.3/bin
export GIT_FOLDER="/d/Java/git"
source $GIT_FOLDER/script-git-dev/bashrc_selsia

alias rm="rm -r"
alias cp="cp -R"