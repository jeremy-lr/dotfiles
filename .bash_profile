# export JAVA_HOME=$(/usr/libexec/java_home)

# ssh -f -N -L:6666:k49.fr.nf:22 e140085r@bastion.etu.univ-nantes.fr
# ssh jeremy@localhost -p 6666
# git clone ssh://jeremy@localhost:6666/srv/git/HQGraphicCompiler.git 


# Proxy = http://www.dsi.univ-nantes.fr/cache.pac
alias projets="cd /Volumes/Data/Projets"
alias rm="rm -r"
alias cp="cp -R"
alias changebg="/Users/jeremy/ChangeBackground.app/Contents/MacOS/applet"

proxy(){
	case $1 in 
		on)
			export http_proxy=http://cache.etu.univ-nantes.fr:3128
			export https_proxy=https://cache.etu.univ-nantes.fr:3128
			sed -ie 's/false/true/g' ~/.m2/settings.xml
			git config --global http.proxy http://E140085E:YNVY3nGU2@cache.etu.univ-nantes.fr:3128 
			export HTTP_PROXY=http://cache.etu.univ-nantes.fr:3128
                        export HTTPS_PROXY=https://cache.etu.univ-nantes.fr:3128

		;;

		off)
			export http_proxy=
			export https_proxy=
			export HTTP_PROXY=
                        export HTTPS_PROXY=
			sed -ie 's/true/false/g' ~/.m2/settings.xml
			git config --global http.proxy ""
		;;

		status)
			if [ -z $(echo ${http_proxy}) ]; then
				echo "Proxy off";
			else
				echo "Proxy on";
			fi;
		;;
	esac;
}

adfile(){
    file=$([ $# -eq 0 ] && echo "." || echo $1)

    git --no-pager diff --exit-code $1

    if [ $? -eq 0 ]; then
        echo -e "No changes in $1\n"
        return;
    fi

    echo

    read "method?Add to staging ? <y/p/n> "

    case $method in
        [yY])
	        git add $file
        ;;

        [pP])
	        git add -p $file
         ;;

        *)
            echo -e "Not added to stagging area\n"
        ;;
    esac
}

# export PATH="$HOME/.jenv/bin:$PATH"
# eval "$(jenv init -)"

alias op='xdg-open'
