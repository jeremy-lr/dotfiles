#!/usr/bin/env bash

source ${GIT_FOLDER}/script-git-dev/bin/colortext.sh

#
# Fonction d'utilisation de notre script
#
function usage() {
	echo "Usage : `basename $0` [-t|--release-train <release-train.conf>] [-n|--dry-run] <ACTION>"
	echo ""
	echo "Actions possibles : check | detect_need_release | prepare | perform | merge | push | status | verify"

	exit -1
}

function execute_action_on_all_folders() {
	action=$1
	shift

	for folder in $*
	do
		execute_action_on_folder ${action} ${folder} || return $?
	done
	return 0
}

function execute_action_on_folder() {
	action=$1
	folder=$2

	opt_dry_run=""
	[ ${DRY_RUN} -eq 1 ] && opt_dry_run="--dry-run"

	release-one.sh ${opt_dry_run} --release-conf ${GIT_FOLDER}/${folder}/release.conf ${action}
	return $?
}

function verify() {
	local wanted_version=""
	read -r -p "$(text yellow "Entrez le numéro de version à vérifier pour ce release train") : " -i ${wanted_version} -e wanted_version
	test -z "$wanted_version" && echo $(text red "La variable WANTED is undefined") && exit -1

	for artifact in ${ARTIFACTS}
	do
		local last_version=$(getlast-version-from-repository ${artifact})
		local color=green
		if [ -z "$last_version" ]; then
			last_version="N/A"
			color="yellow"
		elif [ ${last_version} != ${wanted_version} ]; then
			color="red"
		fi
		printf "%-60s %s\n" ${artifact} $(text ${color} ${last_version})
	done
}

DRY_RUN=0
while true; do
	case "$1" in
		-t | --release-train ) RELEASE_TRAIN_NAME="$2"; shift 2 ;;
		-n | --dry-run      ) DRY_RUN=1; shift ;;
		check | detect_need_release | prepare | perform | merge | push | cancel | status | verify )
			ACTION="$1"; shift ;;
		"") break ;;
		* ) echo $(text red "commande ou option $1 non renconnu") && usage ;;
	esac
done

# On check certaines variables
[ -z ${ACTION} ] && echo $(text red "Il faut préciser une action") && usage
[ -z ${RELEASE_TRAIN_NAME} ] && echo $(text red "Il faut préciser un nom de release train") && usage

# Recherche du fichier de configuration pour notre release train
RELEASE_TRAIN_CONF=${GIT_FOLDER}/script-git-dev/bin/${RELEASE_TRAIN_NAME}-train.conf
test ! -s ${RELEASE_TRAIN_CONF} && echo $(text red "Le fichier de configuration ${RELEASE_TRAIN_CONF} n'existe pas") && usage
source ${RELEASE_TRAIN_CONF}
echo ""
text yellow "$(cat ${RELEASE_TRAIN_CONF} | grep -v "^#")"
echo -e "\n"

# TODO pas très clean ce hack trop spécific. à voir comment on pourrait le redescendre dans le release-one
if [ ${ACTION} = "verify" ]; then
	verify
	exit
fi

# Go ?
ouinon.sh "On lance l'action ${ACTION} pour le release train ${RELEASE_TRAIN}" || exit -1

# En mode préparation, on va récupérer les numéros de version (release et snapshot) qui servira pour l'ensemble du release train
if [ ${ACTION} = "prepare" ]; then
	if [ -n "${ARTIFACT_REFERENCE}" ]; then
		VERSION=$(getlast-version-from-repository ${ARTIFACT_REFERENCE})
	elif [ -n "${FOLDER_REFERENCE}" ]; then
		VERSION=$(extract-project-version ${GIT_FOLDER}/${FOLDER_REFERENCE}/pom.xml)
		VERSION=${VERSION/-SNAPSHOT/}
	fi

	read -r -p "$(text yellow "Entrez le numéro de version pour ce release train") : " -i ${VERSION} -e VERSION
	test -z "$VERSION" && echo $(text red "La variable VERSION is undefined") && exit -1

	read -r -p "$(text yellow "Entrez le prochain numéro de version snapshot pour ce release train (sans le -SNAPSHOT)") : " -i ${SNAPSHOT} -e SNAPSHOT
	test -z "$SNAPSHOT" && echo $(text red "La variable SNAPSHOT is undefined") && exit -1

	# On exporte les variables afin de les propager vers nos différents scripts d'implémentation
	export VERSION
	export SNAPSHOT
fi

if [ ${ACTION} = "detect_need_release" ]; then
	execute_action_on_all_folders "check" ${DEPENDS} || exit -10
	execute_action_on_all_folders "detect_need_release" ${DEPENDS} || exit -15
else
	execute_action_on_all_folders ${ACTION} ${TRAIN_FOLDERS}
fi
