#!/usr/bin/env bash

function prepare_impl() {
	execute git checkout -b ${BRANCH_RELEASE} ${BRANCH_DEV} || return -1

	# On recup le nom de l'artifact depuis le package.json
	ARTIFACT=$(cat package.json | jq -r '.name')
	test -z "$ARTIFACT" && text red "La variable ARTIFACT is undefined" && exit -1
	echo "Artifact is $(text green "$ARTIFACT")"

	if [ -z "${VERSION}" ]; then
		VERSION=$(cat package.json | jq -r '.version')
		VERSION=${VERSION/-SNAPSHOT/}

		read -r -p "$(text yellow "Entrez le numéro de version de release de l'artifact ${ARTIFACT}") : " -i ${VERSION} -e VERSION
		test -z "$VERSION" && echo $(text red "La variable VERSION is undefined") && exit -1

		read -r -p "$(text yellow "Entrez le prochain numéro de version snapshot pour l'artifact ${ARTIFACT} (sans le -SNAPSHOT)") : " -i ${SNAPSHOT} -e SNAPSHOT
		test -z "$SNAPSHOT" && echo $(text red "La variable SNAPSHOT is undefined") && exit -1
	fi

	# On fige la version dans les différents fichier json
	execute sed -i -r "s/\"version\"\s*: \".*\"/\"version\": \"${VERSION}\"/g" package.json
	execute sed -i -r "s/\"number\"\s*: \".*\"/\"number\": \"${VERSION}\"/g" src/app/data/version.json

	# On commit et on tag si ok
	execute git status --porcelain
	execute git diff
	ask "On continue" || exit -1
	execute git add .
	execute git commit -m "$ARTIFACT : Release version $VERSION"
	execute git tag -f -a ${ARTIFACT}-v${VERSION} -m "$ARTIFACT : Release version $VERSION" || return -1
	execute git log --decorate --simplify-by-decoration --oneline --max-count=5

	# On fige la version dans les différents fichier json
	SNAPSHOT="${SNAPSHOT}-SNAPSHOT"
	execute sed -i -r "s/\"version\"\s*: \".*\"/\"version\": \"${SNAPSHOT}\"/g" package.json
	execute sed -i -r "s/\"number\"\s*: \".*\"/\"number\": \"${SNAPSHOT}\"/g" src/app/data/version.json

	# Et on commit en snapshot
	execute git status --porcelain
	execute git diff
	ask "On continue" || exit -1
	execute git add .
	execute git commit -m "$ARTIFACT : Prepare version $SNAPSHOT"

	return 0
}

function perform_impl() {
	# Il faut se replacer sur le commit avec le dernier tag afin d'avoir la version release
	execute git checkout `git describe --tags --abbrev=0`

	execute npm run prune-install || return -1
	execute npm run build || return -1

	# On replace sur le dernier commit afin d'avoir la version snapshot
	execute git checkout ${BRANCH_RELEASE} || return -1

	return 0
}

function merge_impl() {
	execute git checkout ${BRANCH_DEV} || return -1
	execute git merge --ff-only ${BRANCH_RELEASE} || return -1
	execute git branch -d ${BRANCH_RELEASE} || return -1

	return 0
}

function push_impl() {
		execute git checkout ${BRANCH_DEV} || return -1
	execute git push origin ${BRANCH_DEV} || return -1
	execute git push origin `git describe --abbrev=0` || return -1
	lastTag=$(git describe --tags --abbrev=0)

	if [ "${BRANCH_DEV}" != "${BRANCH_PROD}" ]; then
		ask "On repositionne la branche master sur le tag ${lastTag}" || exit -1
		execute git checkout ${BRANCH_PROD} || return -1
		execute git pull || return -1
		execute git reset --hard ${lastTag} || return -1
		execute git push --force origin ${BRANCH_PROD} || return -1

		execute git checkout ${BRANCH_DEV} || return -1
	fi

	return 0
}