#!/bin/bash -e

scriptPath=${GIT_FOLDER}/script-git-dev
gitPath=${GIT_FOLDER}
hooksDirs=$(ls -1d ${gitPath}/*/.git/hooks/)
hooksScripts=$(ls -1d ${scriptPath}/hooks/*)

# conf du template dir
git config --global init.templatedir $HOME/.gittemplates
mkdir -p $HOME/.gittemplates/hooks

function installHooks() {
	destination=$1
	echo -e "Installing hooks into \e[1;32m $destination \e[m"
	for hookScript in ${hooksScripts}; do
		linkName=${destination}/$(basename ${hookScript})
		rm -f ${linkName}
		ln -s ${hookScript} ${linkName}
	done
}

# Installation des hooks dans le gittemplates (valable pour les prochains git clone)
installHooks $HOME/.gittemplates/hooks

# Installation des hooks dans tous les repos existants
for hooksDir in ${hooksDirs}; do
	installHooks ${hooksDir}
done
