#!/bin/bash

MYPATH="$(dirname $0)"
source ${MYPATH}/gitdef

function fetch() {
  local action="Fetch de ${1}"
  encours "$action"
  git fetch --all --prune --tags
  if [ $? -eq 0 ]; then
    ok "$action"
  else
    bad "$action"
    exit 1
  fi
}

if [ ! -d "${GIT_FOLDER}" ]; then
  echo "${GIT_FOLDER} n'existe pas ou n'est pas un dossier"
  exit 1
fi

for d in `ls ${GIT_FOLDER}`; do
  oldcd=`pwd`
  repo="${GIT_FOLDER}/${d}"
  if [ -d "${repo}/.git" ]; then
    echo -e "Dossier ${VERT}$repo${CEND}"
    cd ${repo}
    fetch ${d}
  else
    if [ -d "${repo}" ]; then
      echo -e "Dossier ${ROUGE}$repo${CEND}"
    fi
  fi
  cd $oldcd
done

