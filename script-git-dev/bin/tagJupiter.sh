#!/bin/bash -e

function usage() {
	echo "Usage $0 <version>"
	echo "Usage $0 <version> <repo> <commit>"
}

function tag_one_repo() {
	local version=$1
	local repo=$2
	local commit=$3

	# On se place sur notre repo
	if [ -d $GIT_FOLDER/$repo ]; then
		echo "Tagging $repo"
		cd $GIT_FOLDER/$repo

		# On tag developpement
		git checkout developpement && git tag $version $commit && git push origin $version

		if [ $? -eq 0 ]; then
			# On merge dans master a partir de notre tag
			git checkout master && git pull && git merge --quiet --no-ff $version && git push origin master && git checkout --quiet developpement
		fi
		cd ..
	else
		echo "Le repo GIT $GIT_FOLDER/$repo n'existe pas"
	fi
}

function ask_for_commit() {
	local version=$1
	echo "Tagging version $version"

	read -p "CommitId pour Jupiter : " commit_id_jupiter
	read -p "CommitId pour Applivo : " commit_id_applivo
        read -p "CommitId pour Etat    : " commit_id_etat

	tag_one_repo $version "jupiter" $commit_id_jupiter
	tag_one_repo $version "applivo" $commit_id_applivo
	tag_one_repo $version "etat" $commit_id_etat
}

if [ $# -eq 3 ]; then
	tag_one_repo $*
elif [ $# -eq 1 ]; then
	ask_for_commit $1
else
	usage
	exit -1
fi

