#!/bin/bash -i

source ${GIT_FOLDER}/script-git-dev/bin/colortext.sh

#
# Script permettant de faire les releases chez Selsia via une préparation "locale" sur le poste du dev,
# mais une construction via Jenkins
#
# @author Daco    // Janv 2016
#
# prepare-fixed-version-release-via-jenkins.sh <emplacement du fichier release.conf>
#

# on chk l'existence de notre fichier de conf
RELEASE_CONF=$1
test ! -s ${RELEASE_CONF} && echo $(text red "Le fichier ${RELEASE_CONF} n'existe pas") && exit -1
source ${RELEASE_CONF}

# On check que certaines variables soit bien définis
test -z "${BRANCH_PROD}" && text red "La variable BRANCH_PROD is undefined" && exit -1
test -z "${BRANCH_DEV}" && text red "La variable BRANCH_DEV is undefined" && exit -1

CORPORATE_ARTIFACT="corporate"
grep -q corporate4 pom.xml
if [ "$?" == "0" ]; then
	CORPORATE_ARTIFACT="corporate4"
fi

# un peu d'info
echo $(text yellow "La méthode \"${RELEASE_METHOD}\" va figer automatiquement la version de la façon suivante MAJOR.(MINOR+1).0")
echo $(text yellow "Les propriétés maven définis dans FREEZE_PROPERTIES_VERSION seront figés avec la version en cours de construction")
echo $(text yellow "La génération de l'artifact sera fait avec le job \"Build-Legacy-from-master\" sur Jenkins à partir de la branche ${BRANCH_PROD}")
ouinon.sh "On commence la release ?" || exit -1

# Go
REPO=`basename $(dirname ${RELEASE_CONF})`
gogit ${REPO}

# on checkout la branche de dev
test -n "$(git status --porcelain)" && text red "Attention, il y a du travail en attente" && exit -1
git checkout ${BRANCH_DEV}

# on recup la version du corporate
CORPORATE=$(getlast-version-from-repository $CORPORATE_ARTIFACT)
test -z "$CORPORATE" && text red "La variable CORPORATE is undefined" && exit -1
echo "Corporate version is $(text green "$CORPORATE")"

# On vire le SNAPSHOT de la version finale, on extrait du coup notre version finale et on fige le parent
# On fixe aussi les versions de certaines propriétés la version du projet en cours de release
unlock-project-version
VERSION=$(extract-project-version)
test -z "$VERSION" && text red "La variable VERSION is undefined" && exit -1
echo "Release version is fixed to $(text green "$VERSION")"
freeze-parent-version ${CORPORATE}

for name in ${FREEZE_PROPERTIES_VERSION};
do
	freeze-property-version ${name} ${VERSION}
done
git status --porcelain && git diff pom.xml
ouinon.sh || exit -1

# On commit et on tag
git add . && git commit -m "Release version $VERSION"
git tag -a v${VERSION} -m "Release version $VERSION"

# On prepare en snapshot, on extrait la version snapshot, et remet le parent en RELEASE
# On fixe aussi les version de certaines propriétés avec la nouvelle version snapshot
snapshot-next-project-version
SNAPSHOT=$(extract-project-version)
test -z "$SNAPSHOT" && text red "La variable SNAPSHOT is undefined" && exit -1
echo "Snapshot version is fixed to $(text green "$SNAPSHOT")"
freeze-parent-version "RELEASE"

for name in ${FREEZE_PROPERTIES_VERSION};
do
	freeze-property-version ${name} ${SNAPSHOT}
done
git status --porcelain && git diff pom.xml
ouinon.sh || exit -1

# Et on commit en snapshot
git add . && git commit -m "Prepare version $SNAPSHOT"

# On merge dans notre branche de production à partir de notre tag
# X theirs : on accepte tjs les modifs de la branche principale (cas des pom.xml)
# --no-edit histoire de ne pas avoir l'editeur lors du commit de merge
#ouinon.sh "On merge dans ${BRANCH_PROD}" || exit -1
#git checkout ${BRANCH_PROD} && git pull && git merge --quiet --no-ff v${VERSION} -X theirs --no-edit || exit -1
#git push origin ${BRANCH_PROD} && git push origin v${VERSION} || exit -1

ouinon.sh "On repositionne la branche master sur le tag v${VERSION}" || exit -1
git checkout ${BRANCH_PROD} && git pull
git reset --hard v${VERSION} && git push -f origin master && git push origin v${VERSION} || exit -1

# On fait appel au Job Jenkins qui permet de construire à partir de master
ouinon.sh "On lance le build sur Jenkins" || exit -1
curl -XPOST --user philippe.dacosta:6103fddc303ee3dececa4d06788b4216 "http://ci.selsia.fr/job/Build-Legacy-from-master/buildWithParameters?cause=Release%20${REPO}%20v${VERSION}&token=BUILD_LEGACY_FROM_MASTER_TOKEN&gitrepo=${REPO}" || exit -1

# Afin de récupérer les modifs qui serait sur origin et ne pas perdre nos ID de commit et eviter un push -f, on va faire un commit de merge
ouinon.sh "On switch sur ${BRANCH_DEV}" || exit -1
git checkout ${BRANCH_DEV} && git fetch && git merge origin/${BRANCH_DEV} && git push origin ${BRANCH_DEV}
