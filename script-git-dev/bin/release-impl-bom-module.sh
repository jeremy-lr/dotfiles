#!/usr/bin/env bash

function prepare_impl() {
	execute git checkout -b ${BRANCH_RELEASE} ${BRANCH_DEV} || return -1

	# On recup le nom de l'artifact maven
	ARTIFACT=$(extract-project-artifact)
	test -z "$ARTIFACT" && text red "La variable ARTIFACT is undefined" && exit -1
	echo "Artifact is $(text green "$ARTIFACT")"

	# On va demander le numéro de la version en se basant sur l'existant
	# Dans le cas d'un release-train la version sera renseignée
	if [ -z "${VERSION}" ]; then
		VERSION=$(extract-project-version)
		VERSION=${VERSION/-SNAPSHOT/}
		read -r -p "$(text yellow "Entrez le numéro de version de release de l'artifact ${ARTIFACT}") : " -i ${VERSION} -e VERSION
		test -z "$VERSION" && echo $(text red "La variable VERSION is undefined") && exit -1

		read -r -p "$(text yellow "Entrez le prochain numéro de version snapshot pour l'artifact ${ARTIFACT} (sans le -SNAPSHOT)") : " -i ${SNAPSHOT} -e SNAPSHOT
		test -z "$SNAPSHOT" && echo $(text red "La variable SNAPSHOT is undefined") && exit -1
	fi

	# on recup la version du corporate
	CORPORATE_ARTIFACT=$(extract-parent-artifact)
	test -z "$CORPORATE_ARTIFACT" && text red "La variable CORPORATE_ARTIFACT is undefined" && exit -1
	CORPORATE_PREVIOUS_VERSION=$(extract-parent-version)

	CORPORATE=$(getlast-version-from-repository ${CORPORATE_ARTIFACT})
	test -z "$CORPORATE" && text red "La variable CORPORATE is undefined" && exit -1
	echo "Parent ${CORPORATE_ARTIFACT} version is $(text green "$CORPORATE")"

	# On récupére la version avant la subsitution
	PREVIOUS_VERSION=$(extract-project-version)
	test -z "$PREVIOUS_VERSION" && text red "La variable PREVIOUS_VERSION is undefined" && exit -1

	# On fige la version de l'artifact et du parent
	echo "Release version is fixed to $(text green "$VERSION")"
	execute freeze-project-version ${VERSION}
	execute freeze-parent-version ${CORPORATE}
	# Et on remplace les propriétés entre les tags SPRINT et /SPRINT
	execute sed -i -e "/SPRINT/,/\/SPRINT/ s/${PREVIOUS_VERSION}/${VERSION}/" pom.xml

	# On commit et on tag si ok
	execute git status --porcelain
	execute git diff pom.xml
	ask "On continue" || exit -1
	execute git add .
	execute git commit -m "$ARTIFACT : Release version $VERSION"
	execute git tag -f -a ${ARTIFACT}-v${VERSION} -m "$ARTIFACT : Release version $VERSION" || return -1
	execute git log --decorate --simplify-by-decoration --oneline --max-count=5

	# On fix la prochain version snapshot, on replace le parent sur RELEASE
	SNAPSHOT="${SNAPSHOT}-SNAPSHOT"
	echo "Snapshot version is fixed to $(text green "$SNAPSHOT")"
	execute freeze-parent-version "RELEASE"
	execute freeze-project-version ${SNAPSHOT}
	# Et on remplace les propriétés entre les tags SPRINT et /SPRINT
	execute sed -i -e "/SPRINT/,/\/SPRINT/ s/${VERSION}/${SNAPSHOT}/" pom.xml

	# Et on commit en snapshot
	execute git status --porcelain
	execute git diff pom.xml
	ask "On continue" || exit -1
	execute git add .
	execute git commit -m "$ARTIFACT : Prepare version $SNAPSHOT"

	return 0
}

function perform_impl() {
	# Il faut se replacer sur le commit avec le dernier tag afin d'avoir la version release
	execute git checkout `git describe --tags --abbrev=0`
	execute mvn -B clean deploy || return -1
	execute rm -rf target || return -1

	# On replace sur le dernier commit afin de déployer la version snapshot
	execute git checkout ${BRANCH_RELEASE} || return -1
	execute mvn -B clean deploy || return -1

	return 0
}

function merge_impl() {
	execute git checkout ${BRANCH_DEV} || return -1
	execute git merge --ff-only ${BRANCH_RELEASE} || return -1
	execute git branch -d ${BRANCH_RELEASE} || return -1

	return 0
}

function push_impl() {
	execute git checkout ${BRANCH_DEV} || return -1
	execute git push origin ${BRANCH_DEV} || return -1
	execute git push origin `git describe --abbrev=0` || return -1
	lastTag=$(git describe --tags --abbrev=0)

	if [ "${BRANCH_DEV}" != "${BRANCH_PROD}" ]; then
		ask "On repositionne la branche master sur le tag ${lastTag}" || exit -1
		execute git checkout ${BRANCH_PROD} || return -1
		execute git pull || return -1
		execute git reset --hard ${lastTag} || return -1
		execute git push --force origin ${BRANCH_PROD} || return -1

		execute git checkout ${BRANCH_DEV} || return -1
	fi

	return 0
}