#!/bin/bash

scriptPath=${GIT_FOLDER}/script-git-dev
gitPath=${GIT_FOLDER}
repos=$(ls -1d ${gitPath}/*/.git)

for repo in ${repos}; do
	repo=`dirname ${repo}`
	echo -e "Repo \e[1;32m$repo\e[m"
	cd ${repo}

	currentBranch=$(git rev-parse --symbolic-full-name --abbrev-ref HEAD 2> /dev/null)
	upstream=$(git rev-parse --symbolic-full-name --abbrev-ref "${currentBranch}@{upstream}" 2> /dev/null)

	# fix upstream branch
	if [[ ${currentBranch} != "HEAD" && "origin/$currentBranch" != ${upstream} ]]; then
		if [[ -z ${upstream} ]]; then
			echo git branch --set-upstream-to origin/${currentBranch}
			git branch --set-upstream-to origin/${currentBranch}
		fi
	fi

	# fix conf rebase=true sur les branches locales
	for branch in `git branch -l | cut -c3-`; do
		if [[ -n `git config --local branch.${branch}.remote` ]]; then
			conf=$(git config branch.${branch}.rebase)
			if [[ ${conf} != "true" ]]; then
				echo git config --local branch.${branch}.rebase true
				git config --local branch.${branch}.rebase true
			fi
		fi
	done
done
