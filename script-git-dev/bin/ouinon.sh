#!/bin/bash

msg=${1-On continue}

read -r -p "$msg (o/n) : " -i "o" -e key
if [ "${key}" == "o" ]; then
	exit 0
else
	exit -1
fi