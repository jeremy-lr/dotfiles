#!/bin/bash -le

source ${GIT_FOLDER}/script-git-dev/bin/colortext.sh

#
# Script permettant de faire les releases chez Selsia avec la méthode MAVEN
#
# @author Daco    // Mars 2016
#
# release-via-maven.sh <emplacement du fichier release.conf>
#

# on chk l'existence de notre fichier de conf
RELEASE_CONF=$1
test ! -s ${RELEASE_CONF} && echo $(text red "Le fichier ${RELEASE_CONF} n'existe pas") && exit -1
source ${RELEASE_CONF}

# un peu d'info
echo $(text yellow "Notre méthode de release ""${RELEASE_METHOD}"" va vous demander de préciser la version de la RELEASE et la prochaine version SNAPSHOT")
echo $(text yellow "Attention à la numérotation MAJOR.MINOR.PATCH --> BREAKING.FEATURES.FIX")
ouinon.sh "On commence la release ?" || exit -1

# Go
REPO=$(dirname ${RELEASE_CONF})
cd ${REPO}

# On se fait une branche de release à partir de la branche de dev
BRANCH_RELEASE="release"
git checkout -b ${BRANCH_RELEASE} ${BRANCH_DEV}

# On prepare la release via Maven (change pom.xml sans SNAPSHOT, tag, increment pom.xml avec SNAPSHOT)
mvn release:prepare -DautoVersionSubmodules=true -P coverage
git log -2 --pretty=oneline

# Génération des artifact + deploy
ouinon.sh "On lance la construction des artifacts ?" || (echo $(text yellow "Ne pas oublier de faire du ménage") && exit -1)
mvn -B release:perform -DautoVersionSubmodules=true -P coverage
rm -rf target/checkout

# On merge dans notre branche de dev
ouinon.sh "On merge la branche ${BRANCH_RELEASE} dans ${BRANCH_DEV} ?"
git checkout ${BRANCH_DEV} && git merge --ff-only ${BRANCH_RELEASE} && git branch -d ${BRANCH_RELEASE}
git push origin ${BRANCH_DEV} && git push --tags

# Dans certains cas (module du FW, starter spring boot, il n'y a pas de distinguo entre branche dev et prod
if [ "${BRANCH_DEV}" != "${BRANCH_PROD}" ]; then
	# On merge dans master à partir de notre dernier tag
	lastTag=$(git describe --tags --abbrev=0)
	ouinon.sh "On merge le tag ${lastTag} dans la branche ${BRANCH_PROD} ?"
	git checkout ${BRANCH_PROD} && git pull origin ${BRANCH_PROD} && git merge --no-ff ${lastTag} -m "Merge release tag ${lastTag}"
	git push origin ${BRANCH_PROD}

	# Si tout va bien, on revient sur la branche de dev et on quitte
	git checkout ${BRANCH_DEV}
fi
