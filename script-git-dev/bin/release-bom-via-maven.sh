#!/bin/bash -i

source ${GIT_FOLDER}/script-git-dev/bin/colortext.sh

#
# Script permettant de faire la release d'un BOM maven chez Selsia
#
# @author Daco    // Mars 2017
#

# on chk l'existence de notre fichier de conf
RELEASE_CONF=$1
test ! -s ${RELEASE_CONF} && echo $(text red "Le fichier ${RELEASE_CONF} n'existe pas") && exit -1
source ${RELEASE_CONF}

# On check que certaines variables soit bien définis
test -z "${BRANCH_PROD}" && text red "La variable BRANCH_PROD is undefined" && exit -1
test -z "${BRANCH_DEV}" && text red "La variable BRANCH_DEV is undefined" && exit -1

CORPORATE_ARTIFACT="corporate"
grep -q corporate4 pom.xml
if [ "$?" == "0" ]; then
	CORPORATE_ARTIFACT="corporate4"
fi

# un peu d'info
echo $(text yellow "La méthode \"${RELEASE_METHOD}\" va figer automatiquement la version courante en retirant le -SNAPSHOT mais demandera quel est le prochain numéro de version à utiliser")
ouinon.sh "On commence la release ?" || exit -1

# Go
REPO=`basename $(dirname ${RELEASE_CONF})`
gogit ${REPO}

# on checkout la branche de dev
test -n "$(git status --porcelain)" && text red "Attention, il y a du travail en attente" && exit -1
git checkout ${BRANCH_DEV}

# On recup le nom de l'artifact du BOM
ARTIFACT=$(extract-project-artifact)
test -z "$ARTIFACT" && text red "La variable ARTIFACT is undefined" && exit -1

# on recup la version du corporate
CORPORATE=$(getlast-version-from-repository $CORPORATE_ARTIFACT)
test -z "$CORPORATE" && text red "La variable CORPORATE is undefined" && exit -1
echo "Corporate version is $(text green "$CORPORATE")"

# On récupére la version avant la subsitution
PREVIOUS_VERSION=$(extract-project-version)
test -z "$PREVIOUS_VERSION" && text red "La variable PREVIOUS_VERSION is undefined" && exit -1

# On vire le SNAPSHOT de la version finale, on extrait du coup notre version finale et on fige le parent
unlock-project-version
VERSION=$(extract-project-version)
test -z "$VERSION" && text red "La variable VERSION is undefined" && exit -1

echo "Release version is fixed to $(text green "$VERSION")"
freeze-parent-version ${CORPORATE}

# On remplace les propriétés entre les tags SPRINT et /SPRINT
sed -i -e "/SPRINT/,/\/SPRINT/ s/${PREVIOUS_VERSION}/${VERSION}/" pom.xml
git status --porcelain && git diff pom.xml
ouinon.sh || exit -1

# On commit et on tag
git add . && git commit -m "$ARTIFACT : Release version $VERSION"
git tag -a ${ARTIFACT}-v${VERSION} -m "$ARTIFACT : Release version $VERSION"

# On lance la command emv pour faire le déploiement de la version release
mvn -B clean deploy
rm -rf target

# On prepare en snapshot, on extrait la version snapshot, et remet le parent en RELEASE
#snapshot-next-project-version
SNAPSHOT=$(extract-project-version)
read -r -p "$(text yellow "Enter next developpement version pour l'artifact ${ARTIFACT}") : " -i ${SNAPSHOT} -e SNAPSHOT
test -z "$SNAPSHOT" && text red "La variable SNAPSHOT is undefined" && exit -1

SNAPSHOT="${SNAPSHOT}-SNAPSHOT"
echo "Snapshot version is fixed to $(text green "$SNAPSHOT")"
freeze-parent-version "RELEASE"
freeze-project-version $SNAPSHOT

# On remplace les propriétés entre les tags SPRINT et /SPRINT
sed -i -e "/SPRINT/,/\/SPRINT/ s/${VERSION}/${SNAPSHOT}/" pom.xml
git status --porcelain && git diff pom.xml
ouinon.sh || exit -1

# Et on commit en snapshot
git add . && git commit -m "$ARTIFACT : Prepare version $SNAPSHOT"

# On lance la command emv pour faire le déploiement de la version snapshot
mvn -B clean deploy
rm -rf target

# On push les modifs
git push origin ${BRANCH_PROD} && git push origin ${ARTIFACT}-v${VERSION}
