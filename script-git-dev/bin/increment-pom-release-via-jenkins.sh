#!/bin/bash -l

source ${GIT_FOLDER}/script-git-dev/bin/colortext.sh

#
# Script permettant de faire un hotfix suite à une release via une préparation "locale" sur le poste du dev,
# mais une construction via Jenkins
#
# @author Daco    // Mars 2016
#
# increment-pom-release-via-jenkins.sh <emplacement du fichier release.conf>
#

# on chk l'existence de notre fichier de conf
RELEASE_CONF=$1
test ! -s ${RELEASE_CONF} && echo $(text red "Le fichier ${RELEASE_CONF} n'existe pas") && exit -1
source ${RELEASE_CONF}

# On check que certaines variables soit bien définis
test -z "${BRANCH_PROD}" && text red "La variable BRANCH_PROD is undefined" && exit -1

# un peu d'info
echo $(text yellow "La méthode \"${HOTFIX_METHOD}\" va incrementer la version du pom de la façon suivante MAJOR.MINOR.(PATCH+1)")
echo $(text yellow "La génération de l'artifact sera fait avec le job \"Build-Legacy-from-master\" sur Jenkins à partir de la branche ${BRANCH_PROD}")
ouinon.sh "On commence le hotfix ?" || exit -1

# Go
REPO=$(dirname ${RELEASE_CONF})
cd ${REPO}

# on checkout la branche de prod
test -n "$(git status --porcelain)" && text red "Attention, il y a du travail en attente" && exit -1
git checkout ${BRANCH_PROD}

# On incremente la version MAJOR.MINOR.(PATCH+1)
increment-project-version
VERSION=$(extract-project-version)
test -z "$VERSION" && text red "La variable VERSION is undefined" && exit -1
echo "hotfix version is fixed to $(text green "$VERSION")"

# Pour chaque propriété, on demande la nouvelle version que l'on freeze juste après
for name in ${FREEZE_PROPERTIES_VERSION};
do
	PROP_VERSION=$(extract-property-version ${name})
	read -r -p "$(text yellow "Upgrade version pour la propriéte ${name}") : " -i ${PROP_VERSION} -e NEW_PROP_VERSION
	freeze-property-version ${name} ${NEW_PROP_VERSION}
done

# On affiche le changement du pom.xml
git status --porcelain && git diff pom.xml
ouinon.sh || exit -1

# on commit master, on push
LAST_VERSION=`git describe --abbrev=0`
git add . && git commit -m "`basename ${REPO}` : Upgrade pom.xml suite hotfix de la version $LAST_VERSION"
git push origin ${BRANCH_PROD} || exit -1

# On fait appel au Job Jenkins qui permet de construire à partir de master
curl -XPOST --user philippe.dacosta:6103fddc303ee3dececa4d06788b4216 "http://ci.selsia.fr/job/Build-Legacy-from-master/buildWithParameters?cause=Hotfix%20${REPO}%20v${VERSION}&token=BUILD_LEGACY_FROM_MASTER_TOKEN&gitrepo=`basename ${REPO}`"

# un peu d'info
echo $(text yellow "Ne pas oublier de faire les cherry-pick dans la branche ${BRANCH_DEV}")
git diff --name-status ${LAST_VERSION}