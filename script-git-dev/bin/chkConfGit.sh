#!/bin/bash

scriptPath=${GIT_FOLDER}/script-git-dev
gitPath=${GIT_FOLDER}
repos=$(ls -1d ${gitPath}/*/.git)

for repo in ${repos}; do
	repo=`dirname ${repo}`
	echo -e "Repo \e[1;32m$repo\e[m"
	cd ${repo}
	source ${scriptPath}/hooks/post-checkout
done
