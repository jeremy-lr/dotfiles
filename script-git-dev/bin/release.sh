#!/bin/bash -i

source ${GIT_FOLDER}/script-git-dev/bin/colortext.sh

#
# Script permettant de faire les releases chez Selsia
#
# @author Daco    // Mars 2016
#
# release.sh <repo>
#

function usage() {
	echo "Usage : `basename $0` <nom du repo>"
}

# On check l'existence du repo GIT, et qu'il n'y a pas de travail en attente
REPO=$1
test -z "${REPO}" && usage && exit -1
test ! -d ${GIT_FOLDER}/${REPO} && text red "Le dossier $GIT_FOLDER/$REPO n'existe pas" && exit -1

gogit ${REPO}
test -n "$(git status --porcelain)" && echo $(text red "Attention, il y a du travail en attente dans le repo $REPO") && git status && exit -1

# On cherche un fichier release.conf
RELEASE_CONF=${GIT_FOLDER}/${REPO}/release.conf
test ! -s ${RELEASE_CONF} && text red "Le fichier release.conf n'existe pas dans le repo $REPO" && exit -1

# Verification minimum
source ${RELEASE_CONF}
test -z ${RELEASE_METHOD} && text red "La variable RELEASE_METHOD n'est pas renseignée dans le fichier release.conf" && exit -1
test -z ${BRANCH_DEV} && text red "La variable BRANCH_DEV n'est pas renseignée dans le fichier release.conf" && exit -1
test -z ${BRANCH_PROD} && text red "La variable BRANCH_PROD n'est pas renseignée dans le fichier release.conf" && exit -1

# On determine le script a lancer
if [[ ${RELEASE_METHOD} = "MAVEN" ]]; then
	RELEASE_SCRIPT="release-via-maven.sh"
elif [[ ${RELEASE_METHOD} = "BOM_MAVEN" ]]; then
	RELEASE_SCRIPT="release-bom-via-maven.sh"
elif [[ ${RELEASE_METHOD} = "PREPARE_FIXED_VERSION_THEN_BUILD_WITH_CI" ]]; then
	RELEASE_SCRIPT="prepare-fixed-version-release-via-jenkins.sh"
fi
test -z ${RELEASE_SCRIPT} && text red "Methode de release inconnue : $RELEASE_METHOD " && exit -1

# On affiche le contenu du fichier de conf et on demande la confirmation de lancement
echo $(text green "Lancement du script $RELEASE_SCRIPT avec le fichier de conf : ")
cat ${RELEASE_CONF}

# Go
${RELEASE_SCRIPT} ${RELEASE_CONF}
if [[ $? -eq 0 ]]; then
	echo $(text green "Done")
else
	echo $(text red "Error !!!!!")
fi
