#!/usr/bin/env bash

function prepare_impl() {
	execute git checkout -b ${BRANCH_RELEASE} ${BRANCH_DEV} || return -1
	execute mvn release:prepare -DautoVersionSubmodules=true -P coverage || return -1
	execute git log -2 --pretty=oneline || return -1

	return 0
}

function perform_impl() {
	execute mvn -B release:perform -DautoVersionSubmodules=true -P coverage || return -1
	execute rm -rf target/checkout || return -1

	return 0
}

function merge_impl() {
	execute git checkout ${BRANCH_DEV} || return -1
	execute git merge --ff-only ${BRANCH_RELEASE} || return -1
	execute git branch -d ${BRANCH_RELEASE} || return -1

	return 0
}

function push_impl() {
	execute git checkout ${BRANCH_DEV} || return -1
	execute git push origin ${BRANCH_DEV} || return -1
	execute git push origin `git describe --abbrev=0` || return -1
	lastTag=$(git describe --tags --abbrev=0)

	if [ "${BRANCH_DEV}" != "${BRANCH_PROD}" ]; then
		ask "On repositionne la branche master sur le tag ${lastTag}" || exit -1
		execute git checkout ${BRANCH_PROD} || return -1
		execute git pull || return -1
		execute git reset --hard ${lastTag} || return -1
		execute git push --force origin ${BRANCH_PROD} || return -1

		execute git checkout ${BRANCH_DEV} || return -1
	fi

	return 0
}