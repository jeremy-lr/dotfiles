#!/usr/bin/env bash

source ${GIT_FOLDER}/script-git-dev/bin/colortext.sh
BRANCH_RELEASE="release"

#
# Fonction permettant de savoir si nous sommes clean pour faire une release
# - nous sommes bien dans un repo git ?
# - working directory clean ?
#
function check() {
	start_step "Check"

	git rev-parse --git-dir >/dev/null 2>&1
	test $? -ne 0 \
	&& echo $(text red "Attention, il n'y a pas de repo GIT dans $PWD") \
	&& return -1

	if [ ${RELEASE_METHOD} = "MAVEN" ]; then
		grep -q developerConnection pom.xml
		test $? -ne 0 \
		&& echo $(text red "Impossible de trouver la balise developerConnection dans le fichier $PWD/pom.xml") \
		&& return -1
	fi

	cmd=$(git status --porcelain)
	test -n "${cmd}" \
	&& echo $(text red "Attention, il y a du travail en attente dans le repo $PWD") \
	&& git status \
	&& return -1

	return 0
}

#
# Fonction permettant de savoir s'il faut effectuer une release
# On utilise les commandes git log et git describe pour trouver s'il y a eut des commits depuis la dernière release
#
function detect_need_release() {
	start_step "Detect needed release"

	cmd=$(git lg `git describe --tags --abbrev=0`..HEAD | grep -v "maven-release-plugin" | grep -v "Prepare version" | grep -v "Passage à la version")
	test -n "${cmd}" \
	&& echo $(text red "Attention, il y a des commits en attente de release dans le repo $PWD") \
	&& echo "${cmd}" \
	&& explain_cmd_to_execute "release-one.sh -f ${RELEASE_CONF} --all-actions" \
	&& return -1

	return 0
}

function status() {
	start_step "Status"

	git status

	return 0
}

function prepare() {
	start_step "Prepare release"

	type "prepare_impl" > /dev/null 2>&1 && prepare_impl

	return 0
}

function perform() {
	start_step "Perform release"

	type "perform_impl" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		ask "On lance la construction des artifacts" || return -1
		perform_impl
	fi

	return 0
}

function merge() {
	start_step "Merge release branch"

	type "merge_impl" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		ask "On merge la branche ${BRANCH_RELEASE} dans ${BRANCH_DEV}" || return -1
		merge_impl
	fi
	return 0
}

function push() {
	start_step "Push release"

	type "push_impl" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		ask "On push la branche ${BRANCH_DEV}" || return -1
		push_impl
	fi

	return 0
}

function cancel() {
	execute git reset --hard ${BRANCH_DEV}
	execute git clean -f
	execute git checkout ${BRANCH_DEV}
	execute git show-ref --quiet refs/heads/${BRANCH_RELEASE}
	if [ $? -eq 0 ]; then
		execute git branch -D ${BRANCH_RELEASE}
	fi

	# On supprime le tag qui a pue être posé dans la phase de prepare
	for tag in `git tag --no-merged ${BRANCH_DEV}`
	do
		git tag -d ${tag}
	done

	execute git log --decorate --simplify-by-decoration --oneline --max-count=2
}

#
# Fonction qui consiste à faire l'ensemble des actions
#
function internal_all() {
	check || exit -10
	prepare || exit -20
	perform || exit -30
	merge || exit -40
	push || exit -50
}

function start_step() {
	local mode_dry_run=""
	[ ${DRY_RUN} -eq 1 ] && mode_dry_run=$(text red "DRY-RUN :")
	echo ${mode_dry_run} $(text grey "$@ on $PWD")
}

#
#
#
function execute() {
	# On execute la commande
	if [ ${DRY_RUN} -eq 1 ]; then
		echo "$@"
	else
		"$@"
	fi
	local exit_code=$?
	return ${exit_code}
}

function ask() {
	if [ ${DRY_RUN} -eq 0 ]; then
		ouinon.sh "$@ ?"
		return $?
	else
		echo $(text yellow "Ask question : $@ ?")
	fi
	return 0
}

#
#
#
function explain_cmd_to_execute() {
	echo ""
	echo ""
	echo "Executer la commande suivante : " $(text green $*)
	echo ""
}

#
# Fonction d'utilisation de notre script
#
function usage() {
	echo "Usage : `basename $0` [-A|--all] [-f|--release-conf <release.conf>] [-n|--dry-run] <ACTION>"
	echo ""
	echo "Actions possibles : check | detect_need_release | prepare | perform | merge | push | status"

	exit -1
}

# Parsing des arguments
RELEASE_CONF=${PWD}/release.conf            # On prend le fichier release.conf dans le dossier courant
DRY_RUN=0
while true; do
	case "$1" in
		-A | --all-actions  ) ACTION="internal_all"; shift ;;
		-f | --release-conf ) RELEASE_CONF="$2"; shift 2 ;;
		-n | --dry-run      ) DRY_RUN=1; shift ;;
		check | detect_need_release | prepare | perform | merge | push | cancel | status)
			ACTION="$1"; shift ;;
		"") break ;;
		* ) echo $(text red "commande ou option $1 non renconnu") && usage ;;
	esac
done

# On vérifie qu'il existe une fonction qui correspond à notre action
[ -z ${ACTION} ] && usage
type ${ACTION} > /dev/null 2> /dev/null
[ $? -ne 0 ] && echo "La fonction $(text red ${ACTION}) n'existe pas !!" && usage

# Vérification de l'existence du fichier de configuration pour la release
[ ! -e ${RELEASE_CONF} ] && echo "Le fichier de configuration $(text red ${RELEASE_CONF}) n'existe pas !!" && exit -2

# Sourcing du fichier de conf
source ${RELEASE_CONF}

test -z ${RELEASE_METHOD} && text red "La variable RELEASE_METHOD n'est pas renseignée dans le fichier release.conf" && exit -2
test -z ${BRANCH_DEV} && text red "La variable BRANCH_DEV n'est pas renseignée dans le fichier release.conf" && exit -2
test -z ${BRANCH_PROD} && text red "La variable BRANCH_PROD n'est pas renseignée dans le fichier release.conf" && exit -2

# Détection du script en fonction de la méthode
if [[ ${RELEASE_METHOD} = "MAVEN" ]]; then
	RELEASE_SCRIPT="release-impl-prepare-homemade-perform-maven.sh"
elif [[ ${RELEASE_METHOD} = "BOM_MAVEN" ]]; then
	RELEASE_SCRIPT="release-impl-bom-module.sh"
elif [[ ${RELEASE_METHOD} = "PREPARE_FIXED_VERSION_THEN_BUILD_WITH_CI" ]]; then
	RELEASE_SCRIPT="release-impl-prepare-homemade-perform-via-jenkins.sh"
elif [[ ${RELEASE_METHOD} = "NPM_RELEASE" ]]; then
	RELEASE_SCRIPT="release-impl-with-npm.sh"
fi

test -z ${RELEASE_SCRIPT} && text red "Methode de release inconnue : ${RELEASE_METHOD} " && exit -1

source ${GIT_FOLDER}/script-git-dev/bin/${RELEASE_SCRIPT} \
|| {
	echo $(text red "Le fichier ${RELEASE_SCRIPT} n'existe pas !!")
	exit -1
}

# Lancement de l'action à réaliser
cd `dirname ${RELEASE_CONF}`
${ACTION}
