._bashprofile -> bash shell  
.zshrc -> zsh shell  
jeremy.zsh-theme -> ohmyzsh shell  
.vimrc -> vim text editor  
settings.xml -> maven  
.gitconfig -> git  

```
mkdir -p ~/.fonts/Source_Code_Pro
unzip Source_Code_Pro.zip  -d ~/.fonts/Source_Code_Pro
fc-cache -f
```